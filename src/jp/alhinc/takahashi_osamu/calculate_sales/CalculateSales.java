package jp.alhinc.takahashi_osamu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {


	public static void main(String[] args)  {



		HashMap<String, Long> totalMap = new HashMap<String, Long>();

		HashMap<String, String> faileMap = new HashMap<String, String>();

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}


		if(!read(args[0], "branch.lst",  "支店", "[0-9]{3}",faileMap, totalMap)) {
			return;
		}

		File file = new File(args[0]);

		String[] fileList  = file.list();

		ArrayList<String> fileName = new ArrayList<String>();

		ArrayList<Integer> fileNo = new ArrayList<Integer>();


		for(String fl : fileList) {
			File oneFile = new File((args[0]), fl);
			//  ８桁かつ拡張子.rcdのファイルのみadd
			if((fl.matches("^[0-9]{8}.rcd$")) && (oneFile.isFile())) {
				fileName.add(fl);;
				String[] fn = (fl.split("\\."));
				int flInt = Integer.parseInt(fn[0]);
				fileNo.add(flInt);;
			}
		}
		//  ８桁かつ拡張子.rcdのファイルの数だけループ

		for(int i = 1; i < fileName.size(); i++) {

			if((fileNo.get(i)) - (fileNo.get(i - 1)) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}


		//  売上金額の集計
		BufferedReader br = null;

		try {

			for(int i = 0; i < fileName.size(); i++) {
				//  score(0) = 支店コード, score(1) = 売り上げ金額
				ArrayList<String> score = new ArrayList<String>();

				//  売り上げ集計課題フォルダ内の「８桁」.rcd を読み込み
				File oneFile = new File(args[0],fileName.get(i));
				br  = new BufferedReader(new FileReader(oneFile));
				String line;

				while((line = br .readLine()) != null) {

					score.add(line);;
				}

				int initems = score.size();
				if(initems != 2) {
					System.out.println(fileName.get(i) + "のフォーマットが不正です");
					return;
				}

				if(!score.get(1).matches( "[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long scorel = Long.parseLong(score.get(1));

				if (!totalMap.containsKey(score.get(0))) {
					System.out.println(fileName.get(i) + "の支店コードが不正です");
					return;
				}

				long total = (totalMap.get(score.get(0)) + scorel);

				if(total > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				totalMap.put((score.get(0)), total);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br  != null) {
				try {
					br .close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		if(!write(args[0], "branch.out", faileMap, totalMap)) {
			return;
		}



}


	//  ファイルの読み込み
	public static boolean read(String path, String inFileName, String fileType,  String cordRule, HashMap<String, String> salseFile, HashMap<String, Long>  totalSales) {

		BufferedReader br = null;



		try {

			File file = new File(path, inFileName);

			if(!file.exists()) {
				System.out.println(fileType + "定義ファイルが存在しません");
				return false;
			}


			br = new BufferedReader(new FileReader(file));

			String line;

			while((line = br.readLine()) != null) {

				String[] items = line.split(",");

				if(items.length != 2) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です");
					return false;
				}

				if(!items[0].matches(cordRule)) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です");
					return false;
				}

				long zero = 0;

				salseFile.put(items[0], items[1]);

				totalSales.put(items[0], zero);

			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false ;

		} finally {

			if(br != null) {
				try {
					//  BufferedReaderのclose
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//  ファイルの書き出し
	public static boolean write(String path, String outFileName, HashMap<String, String> salseFileMap, HashMap<String, Long>  totalSalseMap) {

		BufferedWriter bw = null;

		try {

			//  「branch.out」ファイルを売り上げ集計課題フォルダ内に作成
			File fileWriter = new File(path, outFileName);
			bw = new BufferedWriter(new FileWriter(fileWriter));

			//  cord = baranchmap のkey(支店コード)
			for(String cord : salseFileMap.keySet()) {

				//  (branchmap.get(cord)) = 支店名
				//  totalmap.get(cord))  売り上げ合計金額
				//  支店コード + 支店名 + 合計金額
				bw.write(cord + "," + (salseFileMap.get(cord)) + "," + totalSalseMap.get(cord));
				bw.newLine();

			}


		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {

			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}















